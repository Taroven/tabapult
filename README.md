## Tabapult
Tabapult is a Chrome extension that nobody except me probably needs. It's a combination of webserver and Chrome extension that allows you to send tabs and links between computers without needing an external account or service.

The request contents are simply `{'url': '<url>'}` - A simple Flask server is included to receive events. Note that CORS handling is needed, at least from Chrome. Edit `background.js` with the (preferably local) address of your target system - In my case I have the opposite computer in either case mapped to `other.local` in the hosts file, so no changes are needed if I install the extension directly from Git.

## Installing the Extension
Enable developer mode in Chrome's extensions menu, then click "Load unpacked" and point it at the project directory. A new context menu item labeled "Tabapult" will appear for pages and links, clicking it will send the request to the other machine.

## Installing and Running the Server
- On Linux: Install Python's Flask module from pip or your repo, then run the script directly after login. It probably needs $DISPLAY (untested), so adding it during in your desktop's autostart is probably for the best.
- On Windows: Install Python from the Store (or manually and add it to path if the store's not an option), run `pip install flask` in CMD or Terminal, then create a shortcut pointing to `pythonw <script location>` and put it in `shell:startup`. 