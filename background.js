chrome.runtime.onInstalled.addListener(() => {
    chrome.contextMenus.create({
      id: "send-link",
      title: "Tabapult",
      contexts: ["link"]
    });
  
    chrome.contextMenus.create({
      id: "send-page",
      title: "Tabapult",
      contexts: ["page", "frame"]
    });
  });
  
  chrome.contextMenus.onClicked.addListener((info, tab) => {
    let urlToSend = "";
    if (info.menuItemId === "send-link") {
      urlToSend = info.linkUrl;
    } else if (info.menuItemId === "send-page") {
      urlToSend = info.pageUrl;
    }
  
    if (urlToSend) {
      fetch("http://other.local:8088/receive", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ url: urlToSend })
      }).then(response => {
        if (response.ok) {
          console.log("URL sent successfully!");
        } else {
          console.log("Failed to send URL.");
        }
      }).catch(error => {
        console.error("Error:", error);
      });
    }
  });
  