#!/bin/python3
from flask import Flask, request, jsonify
import webbrowser

app = Flask(__name__)


@app.route('/receive', methods=['POST', 'OPTIONS'])
def receive():
    if request.method == 'POST':
        data = request.get_json()
        if not data or 'url' not in data:
            return jsonify({'error': 'Invalid data'}), 400
        url = data['url']
        # Do something with the url
        webbrowser.open(url, autoraise=False)
        return jsonify({'message': 'URL received successfully'}), 200
    else:
        headers = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'POST',
            'Access-Control-Allow-Headers': 'Content-Type'
        }
        return 'success', 204, headers


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8088)
